# Task 2.3 - Claim Button Test

## Project Specific Info

This project tests that the link of the claim button is not broken (returns a 200 OK response code). It also asserts that the URL redirection is relevant
to the best bookmaker suggested. This ensures that the user is redirected to the correct bookmaker.

_Kindly note that second test (correct redirect as per bookmaker) fails since the claim button does not take the user
to the bookmaker's website but opens a new tab with the www.sbo.net homepage instead. **The test relies on the assumption that the user should be redirected to the relevant bookmaker's website, and not to SBO.net again.**_

## Project Overview

The projects written follow a top down approach. The lowest level is the page object. In this class we specify page objects such as web elements and methods which are directly related such as click and send keys. Methods which are more generic or can be used at different instances such as WebDriverWait and WebDriver are found in the Utilities class. This acts as a "mini-framework" for the project itself. 

The page manager level purpose is to join methods found at project level to compile different actions to create scenarios which use multiple methods from the object level. This my not be very useful for such small projects, however, can be critical when having more than one web page per test for example when testing full user journeys. This also applies to the asset manager, which is considered to be on the level as the page manager. The assert manager package contain class/es which do different assertions used objects/methods from the object level or Utilities class in the case of this project. 

Finally, the highest level is the main package, which contains test classes. This is where everything comes together and data is fed to the underlying classes so a functionality can be tested. In this class the TestNG library has been used to structure the tests better by preparing the test prerequisites using the BeforeTest annotation and closing the web driver using the AfterTest annotation. TestNG was used since it can offer visual to the processes/tests running in the background whilst clearly indicating the failed test. 
For both of these projects, test data has been provided by using the VM options and property file. 

This approach was taken to make the project flexible and test data can easily changed/updated without the need of modifying hardcoded data.


## Getting Started

To make use of this project, a ``mvn clean install`` needs to be executed in the terminal to ensure that all dependencies
are downloaded. For simplicity's sake, this project does not make use of frameworks or libraries which are not
available online. 

## Running the tests

1. Clone the project and open it using an IDE.
2. Create a TestNG run/debug configuration. TestKind should be Class and the "CorrectPageTest" class should be selected.
3. Include the browser and website URL in the vm options as shown below. 

``
-Dbrowser=chrome
-DwebsiteURL=https://www.sbo.net/

``

**Kindly note that the supported browser vm options are: chrome, firefox, ie, edge.**