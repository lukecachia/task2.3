package pagemanagers;

import pageobjects.LandingPage;

import static utils.Driver.navigateTo;


/** This class is responsible of
 *  joining all the actions specified
 *  at page object level to create an
 *  end-to-end journey which then results
 *  in a test case.
 */

public class LandingPageManager {

    private static LandingPage landingPage = new LandingPage();

    public static void browseToWebsite(String url){
        navigateTo(url);
    }

    public static void clickBookieSelector(){
        landingPage.clickBookieSelectorBtn();
    }

    public static void selectPreferredOption(String desiredOption) throws InterruptedException {
        landingPage.selectPreferredOptionByTitle(desiredOption);
    }



}
