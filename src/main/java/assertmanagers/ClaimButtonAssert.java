package assertmanagers;

import org.testng.Assert;
import pageobjects.LandingPage;

import java.io.IOException;

import static utils.Utilities.getPropertyValue;
import static utils.Utilities.getResponseForUrl;

/** This class contains methods which assert
 *  required verifications and returns a result
 *  according to the expected result.
 */

public class ClaimButtonAssert {

    private static LandingPage landingPage = new LandingPage();

    /**
     * Method which asserts that the when
     * the URL within the claim button href
     * is called, it returns a 200 OK response code.
     *
     * @throws IOException
     */

    public static void verifyClaimBtnLinkNotBroken() throws IOException {
        int responseCodeReceived = getResponseForUrl(landingPage.getClaimBtnHref());

        try {
            if(responseCodeReceived != 200 ){
                Assert.fail("Claim button link is broken! Got a " + responseCodeReceived + " response code." );
            }
        } catch (NullPointerException e) {
            System.out.println("Ooops! Some parameters must be null.");
        }

    }

    /**
     * Method which asserts that the URL
     * which the user is being redirected to
     * contains the name of the suggested bookmaker
     * to ensure the user is correctly redirected.
     *
     */

    public static void verifyUserIsRedirectedToCorrectBookmaker(){
        try {
            if(!landingPage.getClaimBtnHref().contains(landingPage.getModalBookmakerLogoText())){
                Assert.fail("User was not redirected to the correct bookmaker!");
            }
        } catch (NullPointerException e) {
            System.out.println("Ooops! Some parameters must be null.");
        }

    }



}
