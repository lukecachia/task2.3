package main;

import assertmanagers.ClaimButtonAssert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pagemanagers.LandingPageManager;
import utils.Driver;

import java.io.IOException;


@Test()
public class ClaimButtonTest {

    @BeforeTest
    public void initiateDriverAndSetupPage() {
        Driver.startWebDriver(System.getProperty("browser"));
        LandingPageManager.browseToWebsite(System.getProperty("websiteURL"));
    }

    @Test(testName = "Claim button Link Not Broken Test")
    public void claimBtnLinkNotBroken() throws InterruptedException, IOException {
        LandingPageManager.clickBookieSelector();

        LandingPageManager.selectPreferredOption("Desktop");
        LandingPageManager.selectPreferredOption("€50 - €200");
        LandingPageManager.selectPreferredOption("Yes");

        ClaimButtonAssert.verifyClaimBtnLinkNotBroken();

    }

    @Test(testName = "Claim button Correct Bookmaker Test")
    public void claimBtnRedirectToCorrectBookmaker() {
        ClaimButtonAssert.verifyUserIsRedirectedToCorrectBookmaker();

    }


    @AfterTest
    public void stopWebDriver(){
        Driver.stopWebDriver();
    }



}
