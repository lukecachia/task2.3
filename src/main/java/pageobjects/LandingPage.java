package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utils.Driver;

import java.io.IOException;
import java.util.List;

import static utils.Utilities.*;

/** This class is contains the elements
 *  and other page object found on a specific
 *  web page. Each class within this package
 *  represents a page.
 *
 *  This class also contains actions related
 *  to objects like click and sendKeys.
 */

public class LandingPage {

    private WebDriver driver = Driver.getWebDriver();

    private By bookieSelectorBtn  = By.xpath("/html/body/header/div[1]/div[5]/a");
    private By choiceOption       = By.className("choice-title");
    private By claimBtn           = By.className("claim-button");
    private By bookmakerLogoImg   = By.xpath("//*[@id=\"interactive-wizard\"]/div/div/div/div[2]/div[4]/div[4]/div[2]/div[1]/div[1]/a/img");


    public void clickBookieSelectorBtn(){
        driver.findElement(bookieSelectorBtn).click();

    }

    public void selectPreferredOptionByTitle(String desiredOption) throws InterruptedException {
        Thread.sleep(1000);
        List<WebElement> availableDevices = driver.findElements(choiceOption);

        for(WebElement availableDevice : availableDevices){
            if(availableDevice.getText().equalsIgnoreCase(desiredOption)){
                availableDevice.click();
            }
        }

    }

    public String getClaimBtnHref() {
        return getHrefAttributeContent(driver, claimBtn);
    }

    public String getModalBookmakerLogoText(){
        return driver.findElement(bookmakerLogoImg).getAttribute("alt");
    }



}
